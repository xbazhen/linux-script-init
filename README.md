#123
linux的初始化脚本




项目进度:


注:  
    1. 脚本可自由拷贝和分享  
    2. 建议在测试环境使用,生产环境请做相应修改  
    3. 对于系统而言,默认指最小化安装的系统  
    4. 强烈不建议在云主机(例如阿里云)上使用,因为其已经做了相关初始化操作,例如dns  ntp  yum等设置  
    5. 初始化脚本会自动清除掉 防火墙规则表,并设置禁止开机


系统:  
    CentOS7  
            1. 常见初始化命令(已完成)  
            脚本使用：curl -s https://gitee.com/xbazhen/linux-script-init/raw/master/main.sh | bash -



语言:  
    Python  
        功能:  
            1. 初始化安装源(已完成)  
            2. 安装python3 设置虚拟化环境等(已完成)  
            脚本使用：curl https://gitee.com/xbazhen/linux-script-init/raw/master/language/python/install/CentOS7/main.sh | bash -



