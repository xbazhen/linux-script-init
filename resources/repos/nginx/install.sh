


# curl -s -o /etc/yum.repos.d/CentOS-Base.repo $url_resource/repos/centos/7/CentOS-Base_ustc.repo

curl -s -o /etc/yum.repos.d/nginx.repo https://gitee.com/xbazhen/linux-script-init/raw/master/resources/repos/nginx/nginx.repo

# 辅助命令
# yum list | grep nginx
# yum provides nginx

# centos6
yum install -y nginx-1.12.2-1.el6.ngx.x86_64
chkconfig nginx on
service nginx start

# centos7
# http://nginx.org/packages/centos/7/x86_64/RPMS/nginx-1.12.2-1.el7_4.ngx.x86_64.rpm
yum install -y nginx-1.12.2-1.el7_4.ngx.x86_64
systemctl enable nginx
systemctl start nginx



