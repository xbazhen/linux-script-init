先切换到 root 账户，添加 Key：

su root
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org

为你的系统添加库：
yum install http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm

安装 kernel-ml 包：
yum --enablerepo=elrepo-kernel install kernel-ml

重启：
reboot