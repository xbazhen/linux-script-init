# 安装说明
# https://ius.io/GettingStarted/


# 注: epel-release 是前置软件
rpm -Uvh https://centos7.iuscommunity.org/ius-release.rpm
yum makecache




# 寻找
# 如果1.7.8成为了历史，请开启下面这个
# IUS Community Packages Archive
# yum repolist
# yum --enablerepo=ius-archive provides haproxy


# 安装haproxy  1.7.8  最新的稳定版
# yum --enablerepo=ius-archive install -y haproxy17u-1.7.8-1.ius.centos7.x86_64

yum install -y haproxy17u-1.7.9-1.ius.centos7.x86_64.rpm

# Installing:
#  haproxy17u                                                x86_64                                               1.7.9-1.ius.centos7                                                /haproxy17u-1.7.9-1.ius.centos7.x86_64                                               3.5 M
#  lua53u-libs(依赖包)                                               x86_64                                               5.3.4-1.ius.centos7                                                ius                                                                                  110 k




# # 方式二
# # 直接访问这个网站，查看haproxy 的下载地址
# https://pkgs.org/download/haproxy
# https://centos.pkgs.org/7/ius-x86_64/haproxy17u-1.7.9-1.ius.centos7.x86_64.rpm.html

# cd /usr/local/src
# wget https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64//haproxy17u-1.7.9-1.ius.centos7.x86_64.rpm
# yum localinstall haproxy17u-1.7.9-1.ius.centos7.x86_64.rpm





# 由于这个速度实在太慢，安装完了需要卸载
rpm -e ius-release
