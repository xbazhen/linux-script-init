# Custom bash prompt
export LANG=en_US.UTF8
export EDITOR=vim       # 让kubectl可以高亮

if [[ $(id -u) -eq 0 ]]; then
    export PS1='\n\e[1;37m[\e[m\e[1;32m\u\e[m\e[1;33m@\e[m\e[1;35m\H\e[m:$(pwd)\e[m\e[1;37m]\e[m\e[1;36m\e[m\n# '
else
    export PS1='\n\e[1;37m[\e[m\e[1;32m\u\e[m\e[1;33m@\e[m\e[1;35m\H\e[m:$(pwd)\e[m\e[1;37m]\e[m\e[1;36m\e[m\n$ '
fi

export HISTTIMEFORMAT='[%F %T] '
