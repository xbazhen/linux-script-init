#!/bin/bash
# usage: curl -s https://gitee.com/xbazhen/linux-script-init/raw/master/main.sh | bash -
# TODO: node_exporter

# user: xbazhen
# mail: ...


# 脚本初始化
dir_init=/tmp/xbazhen_init
[[ ! -d $dir_init ]] && mkdir $dir_init
time_Y=$(date +%Y)
time_m=$(date +%m)
time_d=$(date +%d)
time_H=$(date +%H)
time_M=$(date +%M)
time_S=$(date +%S)
time_full=$time_Y-$time_m$time_d-$time_H$time_M$time_S
# echo 当前时间: $time_full

# 判断当前路径
function get_current_path(){
    path1=$(dirname $0)
    first_char=$(echo ${path1:0:1})
    if [[ $first_char == '/' ]]; then
        CURRENT_PATH=$PATH1
    elif [[ $path1 == '.' ]]; then              # 省略了后面的代码,可能比较难理解,但为了代码的整洁,所以省略掉elif [[ $first_char == '.' ]]; then
        CURRENT_PATH=$PWD
    else
        CURRENT_PATH=${PWD}${path1:1}           # 去除掉多余的 .
    fi
}
get_current_path
# 严格分析,应该有三种情况
# 1. 绝对路径
# 2. 相对路径,脚本就在当前目录
# 3. 相对路径,脚本在当前目录的上一级目录






# 一个下载示例
# https://gitee.com/xbazhen/linux-script-init/raw/master/resources/pypi/install.sh
url_project='https://gitee.com/xbazhen/linux-script-init'
url_project_master="$url_project/raw/master"
url_resource="$url_project_master/resources"

# 加载依赖脚本
# source $CURRENT_PATH/main/get_facts/get_system.sh
# curl -s $url_project_master/main/get_facts/get_system.sh | bash -
# 获取发行版信息
function get_dist(){
    ls /etc | grep 'centos-release' >/dev/null 2>&1
    result=$(echo $?)
    if [[ $result == 0 ]]; then
        dist='CentOS'
    else
        dist='Unknow'
    fi
}
get_dist

# 获取CentOS版本信息
function get_centos_version(){
    centos_version=$(cat /etc/centos-release | grep -oP "\d{1,2}\.\d{1,2}")         # 获取详细版本，例如: 6.6   7.3
    centos_version_main=$(echo $centos_version | awk -F . '{print $1}')             # 获取主版本，例如: 6   7
    # echo $centos_version $centos_version_main
}


case $dist in
CentOS)
    get_centos_version
    dist_version=$centos_version
    ;;
*)
esac

# 获取架构信息  暂时不区分 amd64 ia64之类的
function get_arch(){
    getconf LONG_BIT | grep '64' >/dev/null 2>&1
    result=$(echo $?)
    if [[ $result == 0 ]]; then
        arch='64'
    else
        arch='32'
    fi    
}
get_arch


# 留作备份，初始化不启用
function set_hostname(){
    var_hostname='xxx-ip-xxx'
    hostname $var_hostname      # 临时生效，退出会话再登陆有效，一重启就失效。
    if  [[ $centos_version_main == "7" ]]; then
        # 方法1
        # echo $var_hostname > /etc/hostname
        # 方法2
        hostnamectl --static set-hostname $var_hostname
        hostnamectl --transient set-hostname $var_hostname
    elif  [[ $centos_version_main == "6" ]]; then
        sed -i "/^HOSTNAME=/ c\HOSTNAME=$var_hostname" /etc/sysconfig/network  # centos6
    fi
}





# 设置主源(Base源 + epel源 + docker源 + mysql源)
function set_source_centos(){    
    if  [[ $centos_version_main == "7" ]]; then

        # Base-阿里云源，使用总会有问题存在。

        # Base-腾讯云源
        curl -s -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.cloud.tencent.com/repo/centos7_base.repo

        # epel
        yum install -y epel-release # 安装这个，是因为总有些软件要装，然后把地址就改成了epel官方的。
        # epel-腾讯云源
        curl -s -o /etc/yum.repos.d/epel.repo http://mirrors.cloud.tencent.com/repo/epel-7.repo

        # mysql-腾讯云源
        # 官方: https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
        # 下面这个安装，结果是mysql5.6的。
        # rpm -Uvh https://mirrors.ustc.edu.cn/mysql-repo/yum/mysql-5.7-community/el/7/x86_64/mysql-community-release-el7-7.noarch.rpm
        # curl -s -o /etc/yum.repos.d/mysql-community.repo $url_resource/repos/mysql/mysql-community_ustc_tmp1.repo
        
        # postgresql

        # zabbix
    elif [[ $centos_version_main == "6" ]]; then
        # base
        curl -s -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
        # epel
        yum install -y epel-release
        curl -s -o /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-6.repo
    else 
        echo "暂不支持CentOS5及以下的版本, 请手动退出脚本..."
        sleep 1000000000
    fi

    # 至少同时支持6,7的
    yum clean all
    # yum makecache
    yum makecache fast
}

function set_source(){
    if [[ $dist == "CentOS" ]]; then
        set_source_centos
    fi
}



# 设置python源
# 2020年6月6日 现在已经搜索不到python-pip这个包了。
function set_source_pypi(){
    [ ! -d /root/.pip ] && mkdir /root/.pip
    curl -s -o /root/.pip/pip.conf $url_resource/pypi/pip_ali.conf
    yum install -y python3-devel python3-pip python3-setuptools        # 安装默认为9.0.1的版本，而且只有pip3    pip3.6 
    pip3 install --upgrade pip  # 升级成20.1.1，     现在有pip   pip3   pip-3    pip-3.6  pip3.6 这5个。
}

# 安装软件
function install_basic_software(){
    yum install -y \
        net-tools \
        sysstat lsof \
        gcc gcc-c++ \
        expect \
        ntp ntpdate crontabs \
        wget lrzsz tree vim telnet wget openssh-clients unzip zip pigz \
        tomcat-native apr \
        nc net-snmp-utils \
        subversion git \
        bind-utils psmisc \
        man \
        nfs-utils yum-utils
        # nfs-utils 是 /sbin/mount.nfs
        # psmisc 是killall peekfd prtstat pstree fuser

    # python3的一些软件
    # yum update -y
    # yum groupinstall Development tools -y
    # yum -y install zlib-devel
    # yum install -y openssl-devel libxslt-devel libxml2-devel libcurl-devel
    yum install python3 -y 
}

# 设置selinux, 完成后需要重启服务器
function set_selinux(){
    getenforce | grep -i 'enforcing' && setenforce 0
    sed -i '/^SELINUX=/ c\SELINUX=disabled' /etc/selinux/config
    sed -i '/^SELINUX=/ c\SELINUX=disabled' /etc/sysconfig/selinux
}

# set_sudoers
function set_sudoers(){
    sed -i '/^Defaults    requiretty/ c\#Defaults    requiretty' /etc/sudoers
    sed -i '/^Defaults   !visiblepw/ c\Defaults   visiblepw' /etc/sudoers 
}

function set_firewall(){
    if [[ $dist == "CentOS" ]]; then
        # 备份防火墙规则表
        iptables -vnL > $dir_init/firewall@bak$time_full
        if [[ $centos_version_main == "7" ]]; then
            systemctl stop firewalld
            systemctl disable firewalld
        elif [[ $centos_version_main == "6" ]]; then
            service iptables stop
            chkconfig iptables off
        fi        
    fi
}

function get_lanIp(){
    # CentOS7
    # 检测网卡的那里，egrep -A1 'ens[0-9]{1,3}: |eth[0-9]{1,3}: |em[0-9]{1,3}: |bond[0-9]{1,3}: '，可以示单引号，也可以是双引号。centos6 7都支持两种引号
    # enp2s0 是极夜迷你主机的有线网卡设备名
    # 兼容centos6  eth[0-9]{1,3}:{0,1}
    if [[ $centos_version_main == "7" ]]; then
        lanIpALL=$(/sbin/ifconfig | egrep -A1 'enp2s[0-9]{1,3}: |ens[0-9]{1,3}: |eth[0-9]{1,3}: |eno[0-9]{1,3}: |em[0-9]{1,3}: |bond[0-9]{1,3}: ' | sed '/^--/ d' | awk '/inet / {print $2}'| egrep '^10\.|^172\.1[6-9]\.|^172\.2[0-9]\.|^172.3[0-1]\.|^192\.168')
    # CentOS6
    elif [[ $centos_version_main == "6" ]]; then
        lanIpALL=$(/sbin/ifconfig | sed -e '/bond[0-9]:/,/^$/ d' -e '/eth[0-9]:/,/^$/ d' -e '/em[0-9]:/,/^$/ d' | awk '/inet addr:/ {print $2}' | awk -F: '{print $2}' | egrep '^10\.|^172\.1[6-9]\.|^172\.2[0-9]\.|^172.3[0-1]\.|^192\.168')
    fi      

    num_lanIpALL=$(echo $lanIpALL | wc -w)
    if [ "$num_lanIpALL" -eq 1 ] ; then
        lanIp=$lanIpALL
    elif [ "$num_lanIpALL" -eq 0 ] ; then
        echo "未找到内网IP，请检查脚本是否不正确..."
        sleep 1000000000
    else
        echo "Warning! The number of lanIp is not 1(>=2), please choose one and input(IP_LAN1):" # 大于1
        sleep 1000000000
        echo $lanIpALL
        read lanIp
    fi
    # 把获取到的第一个IP写入进去
    echo "export IP_LAN1=$lanIp" > /etc/profile.d/ip_lan1.sh
}

# 设置语言，自定义命令提示符，和histroy日志格式
# 里面有  $(pwd) 只能用文本来保存那段内容 
function set_bash_prompt(){
    curl -s -o /etc/profile.d/bash_prompt.sh $url_resource/other/profile.d/bash_prompt.sh
}
# set_bash_prompt


function set_hosts(){
    local_hosts="$lanIp $HOSTNAME"
    grep "$local_hosts" /etc/hosts >/dev/null || echo "$local_hosts" >> /etc/hosts
    # 查看是否成功
    grep "$local_hosts" /etc/hosts
}

# 设置系统语言
function set_locale(){
    # LANG="zh_CN.UTF-8"    #修改为中文
    # LANG="en_US.UTF-8"    #修改为英文
    if [[ $dist == "CentOS" ]]; then
        if [[ $centos_version_main == "7" ]]; then
            # 方法1
            # sed -i "/^LANG=/ c\LANG=en_US.UTF-8" /etc/locale.conf
            # 方法2，这个也会把上面那个文件修改。
            # localectl set-locale LC_ALL=en_US.UTF-8   # 提示不能给LC_ALL设置
            # localectl set-locale LC_CTYPE=en_US.UTF-8   # 和下面的，会覆盖掉。
            localectl set-locale LANG=en_US.UTF-8
        elif [[ $centos_version_main == "6" ]]; then
            echo "设置系统语言，暂时留空"
        fi
    fi       
}
# https://blog.csdn.net/z4213489/article/details/7937894
# 优先级的关系： LC_ALL>LC_*>LANG   
# 即如果设置了 LANG，其它的都会来继承它。

# 还可以把这个写到 /etc/profile里面
# export LC_ALL="en_US.UTF-8"
# export LC_CTYPE="en_US.UTF-8"
# export LANG="en_US.UTF-8"


# 设置时区为 +8
function set_timezone(){
    cp -f /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    # ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
}

# ntpd
function set_ntpd(){
    if [[ $dist == "CentOS" ]]; then
        if [[ $centos_version_main == "7" ]]; then
            systemctl start ntpd
            systemctl enable ntpd
        elif [[ $centos_version_main == "6" ]]; then
            service ntpd start
            chkconfig ntpd on
        fi
    fi    
}

# crond
function set_crond(){
    if [[ $dist == "CentOS" ]]; then
        if [[ $centos_version_main == "7" ]]; then
            systemctl start crond
            systemctl enable crond
        elif [[ $centos_version_main == "6" ]]; then
            service crond start
            chkconfig crond on
        fi            
    fi    
}

# auto_start
function set_auto_start(){
    if [[ $dist == "CentOS" ]]; then
        if [[ $centos_version_main == "7" ]]
        then
            chmod +x /etc/rc.local
        fi            
    fi    
}


# 设置DNS,以追加模式
function set_dns(){
for dnsserver in "\
nameserver 223.5.5.5
nameserver 114.114.114.114\
"
do 
grep "$dnsserver" /etc/resolv.conf >/dev/null || echo "$dnsserver" >> /etc/resolv.conf
done
}

function set_sshd(){
    # sshd_config
    # 0. 关闭ssh的gssapi认证
    # 1. 关闭远程连接时 DNS 的IP反向解析请求
    # 2. 远程会话时,保持连接
    cfg_file_sshd='/etc/ssh/sshd_config'
    cfg_cmd_nodns='UseDNS no'
    # 主替换命令
    sed -i 's/^GSSAPIAuthentication yes/#GSSAPIAuthentication yes/g' $cfg_file_sshd
    sed -i 's/^GSSAPIDelegateCredentials no/#GSSAPIDelegateCredentials no/g' $cfg_file_sshd
    sed -i '/UseDNS/ c\UseDNS no' $cfg_file_sshd
    # 备用替换命令
    # 配置文件,只检索 'UseDNS' 而不是'UseDNS no' , 因为UseDNS 和no可以不止一个空格
    grep "UseDNS" $cfg_file_sshd >/dev/null || echo "$cfg_cmd_nodns" >> $cfg_sshd
    # ssh客户端保持连接
    sed -i "/^#ClientAliveInterval 0/ c\ClientAliveInterval 60" $cfg_file_sshd
    sed -i "/^#ClientAliveCountMax 3/ c\ClientAliveCountMax 3" $cfg_file_sshd
    
    # 6 7 都通用的
    service sshd reload
    # if [[ $centos_version_main == "7" ]]; then
    #     systemctl reload sshd
    # elif [[ $centos_version_main == "6" ]]; then
    #     service sshd reload
    # fi  
}

function set_dir(){
    # 新建目录
    [ ! -d /apps ] && mkdir /apps
    [ ! -d /data ] && mkdir /data
    [ ! -d /scripts ] && mkdir /scripts
    
}

function set_limits(){
    # 阿里云 和 本地机房 都要优化
echo "\
# Default limit for number of user's processes to prevent
# accidental fork bombs.
# See rhbz #432903 for reasoning.

*          soft    nofile    65535      # 这个一般关注的比较多
root       soft    nofile    unlimited
*          hard    nofile    100000

*          soft    nproc     65535
root       soft    nproc     unlimited
*          hard    nproc     200000\
" > /etc/security/limits.d/20-nproc.conf

    # CentOS6 的默认是 90-nproc.conf
    # CentOS7 的默认是 20-nproc.conf
}

function set_docker(){
# 仅在centos7上安装docker
if [[ $centos_version_main == "7" ]]; then
# 安装docker-compose
yum remove -y docker-compose    # 不移除，下面不会重新安装。
pip install docker-compose

# docker源
# https://mirrors.ustc.edu.cn/docker-ce/linux/centos/docker-ce.repo
# curl -s -o /etc/yum.repos.d/docker-ce.repo $url_resource/repos/docker/7/docker_ustc.repo
# 更换为科大的
# sed -i "s#download.docker.com#mirrors.ustc.edu.cn/docker-ce#g" /etc/yum.repos.d/docker-ce.repo

# 阿里云源
# yum install -y yum-utils device-mapper-persistent-data lvm2
# yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 腾讯云源
docker_service='/etc/systemd/system/multi-user.target.wants/docker.service'
[ -f $docker_service ] && mv -f $docker_service /tmp/docker.service.bak
yum install -y yum-utils device-mapper-persistent-data lvm2
curl -s -o /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo
sudo sed -i 's+download.docker.com+mirrors.cloud.tencent.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo

yum makecache fast

# 安装docker
# https://blog.csdn.net/CSDN_duomaomao/article/details/79171027   k8s支持的docker版本

# https://mirrors.cloud.tencent.com/docker-ce/linux/centos/7/x86_64/stable/Packages/docker-ce-18.09.9-3.el7.x86_64.rpm
# https://mirrors.cloud.tencent.com/docker-ce/linux/centos/7/x86_64/stable/Packages/docker-ce-19.03.13-3.el7.x86_64.rpm
# yum install -y docker-ce-18.09.9-3.el7.x86_64      # 18.09
yum install -y docker-ce-19.03.13-3.el7.x86_64     # 19.03
# yum install -y docker-ce                             # 后面出现了20.10，和k8s暂时不兼容。
               
# 安装的格式
# yum install -y docker-ce-18.09.9-3                 # 不行
# yum install -y docker-ce-18.09.9-3.el7             # 可以
# yum install -y docker-ce-18.09.9-3.el7.x86_64      # 可以
# yum install -y docker-ce-3:18.09.9-3.el7           # 不行
# yum install -y docker-ce-3:18.09.9-3.el7.x86_64    # 可以
# yum install -y docker-ce-18.09.9-3.el7.x86_64      # 18.09


# yum install -y docker-ce    # 永远最新的

# 可能由于/etc/systemd/system/multi-user.target.wants/docker.service和/usr/lib/systemd/system/docker.service不一致。
# Created symlink from /etc/systemd/system/multi-user.target.wants/docker.service to /usr/lib/systemd/system/docker.service.


# 设置配置文件
# 驱动，镜像加速器，内核

# https://registry.docker-cn.com        # 已失效
# 科大      # 有效
# {
#   "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"]
# }
# "registry-mirrors": ["https://zahdqyo7.mirror.aliyuncs.com"],     # 最快的

# {
#    "insecure-registries":["http://172.17.9.81:5000"]
# }

# https://nexus-docker.xbazhen.com  # 家里，在支持v1和匿名用户pull之后，并设置realm之后，成功了。
# https://zhang.ge/5139.html

[ ! -d "/etc/docker" ] && mkdir -p /etc/docker
# [ ! -d "/data/lib/docker" ] && mkdir -p /data/lib/docker  # docker启动会自动创建

# 这个应该是不需要了
#   "storage-opts": [
#     "overlay2.override_kernel_check=true"
#   ],

# 某个公司的？
# https://zahdqyo7.mirror.aliyuncs.com


echo '{
  "registry-mirrors": ["https://zahdqyo7.mirror.aliyuncs.com"],
  "data-root": "/data/docker",
  "storage-driver": "overlay2",
  "exec-opts": ["native.cgroupdriver=systemd"]
}' > /etc/docker/daemon.json

# 很早之前需要，现在不需要了
#   "storage-opts": [
#     "overlay2.override_kernel_check=true"
#   ],

# 注0："registry-mirrors": ["https://registry.docker-cn.com"], 这个
# 注1: overlay2.override_kernel_check=true  这个配置是centos7，3.10的内核才需要。4.0以上内核不需要这个。
# docker 17之后默认是 overlay2？
# 注2: docker的rootdir为 /var/lib/docker，看情况在启动前修改之。


modprobe br_netfilter
echo "\
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
" > /etc/sysctl.d/docker1.conf
sysctl -p /etc/sysctl.d/docker1.conf    

# 设置开机自启动
systemctl enable docker
systemctl start docker
fi
}

function set_ip(){
    # 仅仅做辅助用，虚拟机克隆网卡后无法正常工作时，配置这个。
    echo 'NM_CONTROLLED=no' > /scripts/ifcfg-eth0.bak1
}

# 检查是否已经执行过初始化脚本
function check_init_status(){
    if [[ $INIT_STATUS -ne 1 ]]; then
        echo "该系统已经执行过初始化脚本, 请使用 Ctrl+C 退出脚本..."
        sleep 1000000000
    else
        echo "该系统尚未执行过初始化脚本，请等待进一步检测..."
    fi
}

# 设置初始化状态，执行完毕之后，状态为1
function set_init_status(){
    echo "export INIT_STATUS=1" > /etc/profile.d/init_statu.sh
}

# 主函数
function main(){
    # check_init_status

    # 系统信息展示
    echo "本机系统为: ${dist}${dist_version} , 系统架构为: ${arch}位!"
    sleep 1

    # 检查此脚本是否兼容被初始化的系统
    support_dist="CentOS"
    echo $support_dist | grep $dist
    if [[ $? -ne 0 ]]; then
        echo "该初始化脚本暂不支持此发行版, 请使用 Ctrl+C 退出脚本..."
        sleep 1000000000
    else
        echo "系统兼容性检测通过, 开始执行初始化脚本..."
    fi
    
    # echo "\
    # +---------------------------------------+
    # |   your system is CentOS 6 x86_64      |
    # |        start optimizing.......        |
    # +---------------------------------------+
    # "

    set_source          # 阿里云需要注释掉
    set_source_pypi     # 阿里云需要注释掉
    install_basic_software
    get_lanIp           # 在安装好 net-tools后， 在获取局域网ip
    set_selinux
    set_sudoers
    set_firewall
    set_bash_prompt
    set_hosts
    set_locale
    set_timezone
    set_ntpd
    set_crond
    set_auto_start
    set_dns     #这个看情况, 网关可以强制DNS代理
    set_sshd
    set_dir
    set_limits
    docker info || set_docker       # 不重复安装docker
    set_ip
    set_init_status

    echo "\
    +-------------------------------------------------+
    |               optimizer is done                 |
    |   it's recommond to restart this server !       |
    +-------------------------------------------------+
    "
}
main




# 需要重启才能生效的有以下几个
# set_locale




