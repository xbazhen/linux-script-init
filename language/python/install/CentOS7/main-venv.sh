#!/bin/bash
# usage: curl https://gitee.com/xbazhen/linux-script-init/raw/master/language/python/install/CentOS7/main.sh | bash -



# 注：仅在CentoO7.3测试通过，在其他系统可能会遇到各种问题，请自行解决~




# 前置软件包1:
yum clean all
yum install -y wget gcc zlib-devel bzip2-devel openssl-devel ncurses-devel readline-devel soci-sqlite3-devel


# 下载
cd /usr/local/src
wget -c https://www.python.org/ftp/python/3.6.10/Python-3.6.10.tgz    # http://mirrors.sohu.com/python/3.6.10/Python-3.6.10.tgz 搜狐已下架python安装
tar -zxf Python-3.6.10.tgz

# 清除历史安装，如果重复
[ -d "/usr/local/python3.6" ] && rm -rf /usr/local/python3.6

# 编译安装，增加多核编译效率 make -j
cd Python-3.6.10
./configure --prefix=/usr/local/python3.6 --with-ensurepip=install
make -j "$(nproc)" && make install



# 4.1 设置root用户的(提前设置，不然很慢)
cd /root 
[ ! -d ".pip" ] && mkdir .pip
echo "\
[global]
index-url = https://pypi.doubanio.com/simple

[install]
trusted-host=pypi.doubanio.com\
" > ./.pip/pip.conf


# 安装virtualenv和新版本pip
/usr/local/python3.6/bin/python3.6 -m pip install --upgrade pip
# 软链，全局存在时，加上版本号和"-g"字符串，以免混淆。
ln -sv /usr/local/python3.6/bin/python3.6     /usr/bin/python3.6-g
ln -sv /usr/local/python3.6/bin/pip3.6        /usr/bin/pip3.6-g


# 创建python用户
groupadd pyadmin
useradd py36venv -g pyadmin
echo 'py36venv:123456' | chpasswd



# 2.1 开始为python3创建venv,其它python版本同理
sudo -H -u py36venv bash -c '/usr/local/python3.6/bin/python3.6 -m venv ~/venv'                 # ./venv表示目录
sudo -H -u py36venv bash -c '~/venv/bin/python3.6 -m pip install --upgrade pip'   # 升级下
# 创建后的虚拟环境目录为: /home/py36venv/venv

# 一个用户，多个目录的建议。
# $ source ~/envs/tutorial-env/bin/activate


# 3.1 设置py36登录自动激活virtualenv（二选一）
# 默认为 PATH=$PATH:$HOME/.local/bin:$HOME/bin
cp /home/py36venv/.bash_profile /home/py36venv/.bash_profile.$(date +%Y%m%d_%H%M%S)
echo '# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs
export PATH

source ~/venv/bin/activate
' > /home/py36venv/.bash_profile

# 创建编码目录
# 创建应用发布目录
[ ! -d "/home/py36venv/code" ] && mkdir -p /home/py36venv/code
[ ! -d "/home/py36venv/deploy" ] && mkdir -p /home/py36venv/deploy

# 不退出用户,手动执行加载
# source ~/.bash_profile

# 修改用户所有权
chown -R py36venv.pyadmin /home/py36venv



# 4.2 设置 py36venv 用户的
# XDG_CONFIG_HOME
cd /home/py36venv
[ ! -d ".config/pip/" ] && mkdir .config/pip/
echo "\
[global]
index-url = https://pypi.doubanio.com/simple

[install]
trusted-host=pypi.doubanio.com\
" > .config/pip/pip.conf

# 每个用户
# 1). 新的 $HOME/.config/pip/pip.conf  或 XDG_CONFIG_HOME
# 2). 旧的 $HOME/.pip/pip.conf 或 PIP_CONFIG_FILE
# 在virtualenv内部
# $VIRTUAL_ENV/pip.conf
# 整个网站
# 1). /etc/pip.conf
# 2). XDG_CONFIG_DIRS（如果存在）中 设置的任何路径的“ pip”子目录中 /etc/xdg/pip/pip.conf

# 如果通过pip找到了多个配置文件，则它们将按以下顺序组合：
# 1. 读取站点范围的文件
# 2. 读取每个用户的文件
# 3. 读取特定于virtualenv的文件  # 优先级最高
# https://pip.pypa.io/en/stable/user_guide/

echo "\

-----------------------------------------------------------
|   python的专用开发用户                                  |
|   Linux用户名：py36venv                                |
|   密码: 123456                                          |
|   可以使用： su - py36venv 来一键切换到python环境      |
|   切换py36venv后，pip安装的软件，均在本用户的家目录下  |
-----------------------------------------------------------

"


