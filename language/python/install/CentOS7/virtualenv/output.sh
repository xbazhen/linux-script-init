教程文档
http://pythonguidecn.readthedocs.io/zh/latest/dev/virtualenvs.html


这里是要为3个用户创建
useradd py35admin ; echo 'py35admin:py35admin' | chpasswd
useradd py27admin ; echo 'py27admin:py27admin' | chpasswd


0. virtualenv 只需要在以上3个版本任意一个的pip安装就好    
就选择yum包安装的那个pip吧
pip install virtualenv
[py35admin@L170-BlueKing ~]$ virtualenv --version
15.1.0
[py35admin@L170-BlueKing ~]$ whereis virtualenv
virtualenv: /usr/bin/virtualenv

注: 由于yum的virtualenv版本太低,故不选择yum的

0. 切换用户,二选一执行
su - py35admin
su - py27admin

1.1 修改pip, 使用阿里云
cd ~
[ ! -d ".pip" ] && mkdir .pip
echo "\
[global]
index-url = https://mirrors.aliyun.com/pypi/simple/

[install]
trusted-host=mirrors.aliyun.com" > ~/.pip/pip.conf

1.2 修改pip, 使用豆瓣
cd ~
[ ! -d ".pip" ] && mkdir .pip
echo "\
[global]
index-url = https://pypi.doubanio.com/simple/

[install]
trusted-host=pypi.doubanio.com
" > ~/.pip/pip.conf


2. 开始为python3.5.3创建,其它python版本同理
cd ~
virtualenv -p /usr/local/python3.5/bin/python3 venv
virtualenv -p /usr/local/python2.7/bin/python2 venv

创建后的虚拟环境目录为: /home/py35admin/venv


3. 设置py35登录自动激活virtualenv
vim ~/.bash_profile
添加source,在PATH之前
source ~/venv/bin/activate
PATH=$PATH:$HOME/bin:$HOME/venv/bin            # 默认为 PATH=$PATH:$HOME/bin

# 不退出用户,手动执行加载
source ~/.bash_profile



4. 依次为python27添加虚拟环境

注1： 这个编译安装python2.7的时候，即使没有安装setuptools，设置虚拟环境之后， 仍然会有pip， 推测是virtualenv从python3.5那边弄过来的，但是适用于python2.7

Successfully installed django-1.8.18
(venv) [py27admin@L170-BlueKing ~]$ cd bin
-bash: cd: bin: No such file or directory
(venv) [py27admin@L170-BlueKing ~]$ cd venv/bin/
(venv) [py27admin@L170-BlueKing bin]$ ls -l
total 6172
-rw-rw-r-- 1 py27admin py27admin    2079 Apr 17 11:31 activate
-rw-rw-r-- 1 py27admin py27admin    1021 Apr 17 11:31 activate.csh
-rw-rw-r-- 1 py27admin py27admin    2175 Apr 17 11:31 activate.fish
-rw-rw-r-- 1 py27admin py27admin    1137 Apr 17 11:31 activate_this.py
-rwxrwxr-x 1 py27admin py27admin     282 Apr 17 11:35 django-admin
-rwxrwxr-x 1 py27admin py27admin     140 Apr 17 11:35 django-admin.py
-rw-rw-r-- 1 py27admin py27admin     305 Apr 17 11:35 django-admin.pyc
-rwxrwxr-x 1 py27admin py27admin     249 Apr 17 11:31 easy_install
-rwxrwxr-x 1 py27admin py27admin     249 Apr 17 11:31 easy_install-2.7
-rwxrwxr-x 1 py27admin py27admin     221 Apr 17 11:31 pip
-rwxrwxr-x 1 py27admin py27admin     221 Apr 17 11:31 pip2
-rwxrwxr-x 1 py27admin py27admin     221 Apr 17 11:31 pip2.7
-rwxrwxr-x 1 py27admin py27admin 6261544 Apr 17 11:31 python
lrwxrwxrwx 1 py27admin py27admin       6 Apr 17 11:31 python2 -> python
lrwxrwxrwx 1 py27admin py27admin       6 Apr 17 11:31 python2.7 -> python
-rwxrwxr-x 1 py27admin py27admin    2338 Apr 17 11:31 python-config
-rwxrwxr-x 1 py27admin py27admin     228 Apr 17 11:31 wheel
(venv) [py27admin@L170-BlueKing bin]$ 


注2：即使没有安装readline-devel，在py27admin家目录下的虚拟环境，仍然可以按backspace删除，而不需要使用ctrl+backspace来删除


