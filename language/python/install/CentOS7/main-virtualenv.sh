#!/bin/bash
# usage: curl https://gitee.com/xbazhen/linux-script-init/raw/master/language/python/install/CentOS7/main.sh | bash -



# 注：仅在CentoO7.3测试通过，在其他系统可能会遇到各种问题，请自行解决~




# 前置软件包1:
yum clean all
yum install -y wget gcc zlib-devel bzip2-devel openssl-devel ncurses-devel readline-devel soci-sqlite3-devel


# 下载
cd /usr/local/src
wget -c https://www.python.org/ftp/python/3.6.10/Python-3.6.10.tgz    # http://mirrors.sohu.com/python/3.6.10/Python-3.6.10.tgz 搜狐已下架python安装
tar -zxf Python-3.6.10.tgz

# 清除历史安装，如果重复
[ -d "/usr/local/python3.6" ] && rm -rf /usr/local/python3.6

# 编译安装，增加多核编译效率 make -j
cd Python-3.6.10
./configure --prefix=/usr/local/python3.6 --with-ensurepip=install
make -j "$(nproc)" && make install



# 4.1 设置root用户的(提前设置，不然很慢)
cd /root 
[ ! -d ".pip" ] && mkdir .pip
echo "\
[global]
index-url = https://pypi.doubanio.com/simple

[install]
trusted-host=pypi.doubanio.com\
" > ./.pip/pip.conf


# 安装virtualenv和新版本pip
/usr/local/python3.6/bin/pip install virtualenv
/usr/local/python3.6/bin/pip install --upgrade pip

# 软链，全局存在时，加上版本号和"-g"字符串，以免混淆。
ln -sv /usr/local/python3.6/bin/virtualenv /usr/bin/virtualenv3.6-g
ln -sv /usr/local/python3.6/bin/pip        /usr/bin/pip3.6-g


# 创建python用户
groupadd pyadmin
useradd py36venv -g pyadmin
echo 'py36venv:123456' | chpasswd



# 2.1 开始为python3创建venv,其它python版本同理
sudo -H -u py36venv bash -c 'cd ~;/usr/local/python3.6/bin/virtualenv -p /usr/local/python3.6/bin/python3.6 venv'   # 根目录，可以开个venv，也可以不开。
# 创建后的虚拟环境目录为: /home/py36venv/venv

# venv，这个在一些Python新版本上才有。老版本就需要使用virtualenv
# 一个等价的命令：python -m venv tutorial-env
# $ source ~/envs/tutorial-env/bin/activate


# 3.1 设置py36登录自动激活virtualenv（二选一）
# 默认为 PATH=$PATH:$HOME/.local/bin:$HOME/bin
cp /home/py36venv/.bash_profile /home/py36venv/.bash_profile.$(date +%Y%m%d_%H%M%S)
echo '# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs
export PATH

source ~/venv/bin/activate
' > /home/py36venv/.bash_profile

# 创建编码目录
# 创建应用发布目录
[ ! -d "/home/py36venv/code" ] && mkdir -p /home/py36venv/code
[ ! -d "/home/py36venv/deploy" ] && mkdir -p /home/py36venv/deploy

# 不退出用户,手动执行加载
# source ~/.bash_profile

# 修改用户所有权
chown -R py36venv.pyadmin /home/py36venv



# 4.2 设置 py36venv 用户的
cd /home/py36venv
[ ! -d ".pip" ] && mkdir .pip
echo "\
[global]
index-url = https://pypi.doubanio.com/simple

[install]
trusted-host=pypi.doubanio.com\
" > ./.pip/pip.conf



echo "\

-----------------------------------------------------------
|   python的专用开发用户                                  |
|   Linux用户名：py36venv                                |
|   密码: 123456                                          |
|   可以使用： su - py36venv 来一键切换到python环境      |
|   切换py36venv后，pip安装的软件，均在本用户的家目录下  |
-----------------------------------------------------------

"


