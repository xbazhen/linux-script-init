#!/bin/bash



# 原始数据来源
# https://www.iviewui.com/docs/guide/iview-loader


# 由于部分数据，例如'i-form-item'有'FormItem'和'Form-item'两种，所以会增加以下描述.


file_lable='iviewH5LabelInfo.csv'

curl -s "https://gitee.com/xbazhen/linux-script-init/raw/master/language/vue/iview/$file_lable" > $file_lable

lines=$(wc -l $file_lable | awk '{print $1}')
echo "开始执行 iview的H5标签标准化脚本: ..."
for line in $(seq 1 $lines)
do
    line_info=$(sed -n "$line p" $file_lable)
    target_name=$(echo $line_info | awk -F',' '{print $1}')
    src1_name=$(echo $line_info | awk -F',' '{print $2}')
    src2_name=$(echo $line_info | awk -F',' '{print $3}')  
    # 打印相关变量信息
    # echo "本行为：$line, target_name是：$target_name, src1_name是: $src1_name, src2_name是: $src2_name"
    # 要替换的名称，不为空，才进行替换
    if [ ! -z $target_name ]; then
        # 替换第二个域为第一个域
        if [ ! -z $src1_name ]; then
            # echo "$src1_name not null"
            find ./src -name "*.vue" -type f | xargs sed -i -e "s#<${src1_name} #<${target_name} #g" -e "s#<${src1_name}>#<${target_name}>#g" -e "s#</${src1_name}>#</${target_name}>#g"
        fi
        # 替换第三个域为第一个域
        if [ ! -z $src2_name ]; then
            # echo "$src2_name not null"
            find ./src -name "*.vue" -type f | xargs sed -i -e "s#<${src2_name} #<${target_name} #g" -e "s#<${src2_name}>#<${target_name}>#g" -e "s#</${src2_name}>#</${target_name}>#g"
        fi        
    fi
done
echo "脚本执行完毕！！！"

# 为了防止替换错误，例如替换第一组会导致第二组替换，脚本对此做了严格校验
# i-form,Form,
# i-form-item,FormItem,Form-item

# 测试命令，前两个是开始标签，最后个是结束标签
# find ./src -name "*.vue" -type f | xargs sed -i -e "s#<Form-item #<i-form-item #g" -e "s#<Form-item>#<i-form-item>#g" -e "s#</Form-item>#</i-form-item>#g"

