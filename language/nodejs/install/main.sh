


# 可以看到安装各个平台的
https://nodejs.org/en/download/package-manager/

# CentOS7
# 已过期
# https://nodejs.org/en/download/package-manager/#enterprise-linux-and-fedora
# 有效
https://github.com/nodesource/distributions/blob/master/README.md

# 设置源
#curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
curl -sL https://rpm.nodesource.com/setup_12.x | bash -

# 可选
# sudo yum install gcc-c++ make

sudo yum -y install nodejs


# 查看npm的版本
[root@L101-207-Vap-Walle yum.repos.d]# npm --version
5.3.0


# ----------------------------------------------------------------------------------
# 优化中国区访问源


http://npm.taobao.org/


# 安装cnpm
npm install -g cnpm --registry=https://registry.npm.taobao.org


# 查看cnpm的版本
[root@L101-207-Vap-Walle yum.repos.d]# cnpm --version
cnpm@5.1.1 (/usr/lib/node_modules/cnpm/lib/parse_argv.js)
npm@5.4.2 (/usr/lib/node_modules/cnpm/node_modules/npm/lib/npm.js)
node@8.5.0 (/usr/bin/node)
npminstall@3.1.4 (/usr/lib/node_modules/cnpm/node_modules/npminstall/lib/index.js)
prefix=/usr 
linux x64 3.10.0-514.el7.x86_64 
registry=http://registry.npm.taobao.org


